package com.example.zelda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.zelda.R
import com.example.zelda.TradeResponse
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat

class DataRecycleViewAdapter(
    private val context: Context,
    private val dataList: MutableList<TradeResponse>?
): RecyclerView.Adapter<DataRecycleViewAdapter.PageHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PageHolder {
        return PageHolder(LayoutInflater.from(context).inflate(R.layout.recycle_item, parent, false))
    }

    override fun onBindViewHolder(holder: PageHolder, position: Int) {
//        val realPosition = position
        holder.timeItem.text = stampToDate(dataList!![position].time.toLong()).split(" ")[1]
        holder.priceItem.text = getNoMoreThanTwoDigits(dataList[position].price.toDouble())
        if (dataList[position].isAuto)  holder.priceItem.setTextColor(ContextCompat.getColor(context, R.color.red))
        else holder.priceItem.setTextColor(ContextCompat.getColor(context, R.color.green))
        holder.amountItem.text = getNoMoreThanSixDigits(dataList[position].amount.toDouble())
    }

    override fun getItemCount(): Int {
        return dataList!!.size
    }

    inner class PageHolder(view: View): RecyclerView.ViewHolder(view){
        val timeItem: TextView = view.findViewById(R.id.timeItem)
        val amountItem: TextView = view.findViewById(R.id.amountItem)
        val priceItem: TextView = view.findViewById(R.id.priceItem)
    }


    private fun stampToDate(time: Long): String {
        return SimpleDateFormat("yyyy-DD-MM hh:mm:ss").format(time)
    }

    private fun getNoMoreThanTwoDigits(number: Double): String {
        val format = DecimalFormat("0.##")
        //未保留小數的捨棄規則，RoundingMode.FLOOR表示直接捨棄。
        format.roundingMode = RoundingMode.FLOOR
        return format.format(number)
    }

    private fun getNoMoreThanSixDigits(number: Double): String {
        val format = DecimalFormat("0.######")
        //未保留小數的捨棄規則，RoundingMode.FLOOR表示直接捨棄。
        format.roundingMode = RoundingMode.FLOOR
        return format.format(number)
    }


    fun updateData(data : TradeResponse) {
        dataList?.add(0, data)
        notifyItemInserted(0)
        dataList?.removeAt(dataList.lastIndex)
        notifyItemRemoved(dataList!!.lastIndex+1)
    }
}