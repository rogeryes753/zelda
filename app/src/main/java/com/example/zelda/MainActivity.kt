package com.example.zelda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zelda.adapter.DataRecycleViewAdapter
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    var tradeResponse : MutableList<TradeResponse>? = null
    var newData : TradeResponse? = null
    var adapter : DataRecycleViewAdapter? = null
    var task = Runnable {}
    var socket : WebSocket? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val symbol = "BTCUSDT"
        val wsUrl = "wss://stream.yshyqxx.com/ws/btcusdt@aggTrade"
        val apiService = APIClient.client.create(CloudAPI::class.java)
        val recyclerView : RecyclerView? = this.findViewById(R.id.dataRecyclerView)
        apiService.data(symbol).enqueue(object :
            Callback<MutableList<TradeResponse>> {
            override fun onResponse(call: Call<MutableList<TradeResponse>>, response: Response<MutableList<TradeResponse>>) {
                if (response.isSuccessful) {
                    tradeResponse = response.body()
                    Log.d("tradeResponse", "$tradeResponse")
                    val allData : MutableList<TradeResponse>? = tradeResponse?.subList(tradeResponse!!.size -41, tradeResponse!!.lastIndex)?.asReversed()
                    setRecyclerView(recyclerView, allData)
                    startWS(wsUrl)
                    Handler().postDelayed(task, 1000)
                }
            }

            override fun onFailure(call: Call<MutableList<TradeResponse>>, t: Throwable) {
                Log.d("tradeResponse", "$call and t : $t")
            }

        })

        task = Runnable {
            if (newData != null) {
                Log.d("newData", "$newData")
                adapter?.updateData(newData!!)
            }
            newData = null
            Handler().postDelayed(task, 1500)
        }

    }

    private fun startWS(wsUrl : String) {
        val client = OkHttpClient.Builder()
            .readTimeout(3, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build()

        val request = Request.Builder()
            .url(wsUrl)
            .build()

        val wsCallBack = object : WebSocketListener() {
            override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                super.onClosed(webSocket, code, reason)
            }


            override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
                super.onClosing(webSocket, code, reason)
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: okhttp3.Response?) {
                super.onFailure(webSocket, t, response)
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                super.onMessage(webSocket, text)
                Log.d("tradeResponse", "$text")
                if (text == "ping") socket?.send("pong")
                else newData = Gson().fromJson(text, TradeResponse::class.java)

            }

            override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
                super.onMessage(webSocket, bytes)
            }

            override fun onOpen(webSocket: WebSocket, response: okhttp3.Response) {
                super.onOpen(webSocket, response)
            }
        }
        socket = client.newWebSocket(request, wsCallBack)
    }


    fun setRecyclerView(recyclerView: RecyclerView?, tradeResponse: MutableList<TradeResponse>?) {
        val mLayoutManager = GridLayoutManager(this, 1)
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        adapter = DataRecycleViewAdapter(this, tradeResponse)
        recyclerView?.layoutManager = mLayoutManager
        recyclerView!!.adapter = adapter
    }
}