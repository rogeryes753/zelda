package com.example.zelda
import com.google.gson.annotations.SerializedName

data class TradeResponse(
    @SerializedName("a")
    val Id : Number,
    @SerializedName("p")
    val price : String,
    @SerializedName("q")
    val amount : String,
    @SerializedName("f")
    val firstId : Number,
    @SerializedName("l")
    val lastId : Number,
    @SerializedName("T")
    val time : Number,
    @SerializedName("m")
    val isAuto : Boolean,
    @SerializedName("M")
    val isBest : Boolean,
)
